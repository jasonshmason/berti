<?php

// {"set": [
		// { 
		// 	 "c" : "K&L" ,
		// 	 "a" : {"0" : "(D&K)&J", "1" : "L&P" } ,
		// 	 "s" : {"0" : "D&K 1 &E", "1" : "K 3 &E", "2" : "L 2 &E", "3" : "K&L 4,5 &I" },
		// 	 "h" : {"0" : "The conclusion is a conjunction", "1" : "Use conjunction introduction at the last step", "2" : "First use conjunction elimination on the premises" }
		// },
		// { 
		// 	 "c" : "K&L" ,
		// 	 "a" : {"0" : "(D&K)&J", "1" : "L&P" } ,
		// 	 "s" : {"0" : "D&K 1 &E", "1" : "K 3 &E", "2" : "L 2 &E", "3" : "K&L 4,5 &I" },
		// 	 "h" : {"0" : "The conclusion is a conjunction", "1" : "Use conjunction introduction at the last step", "2" : "First use conjunction elimination on the premises" }
		// }
// 	]}

$read = "PROBS1.BER";
$write="../json/prob1.json";

$lastLine = count(file($read));

$rh = fopen($read, 'r');
$wh = fopen($write, 'w');

#$probCount=1;

$openSet	=	'{"set": ['	."\r\n";
$closeSet	=	']}'		."\r\n";

//$openProb		=	"\t".		'{"p" : ['	."\r\n";
$openProb		=	"\t".		'{'	."\r\n";
$closeProb		=	"\t".		'},'."\r\n";
$closeProbFin	=	"\t".		'}'."\r\n";

$assume=$solve=$hint=array();

$fwrite		=	fwrite($wh, $openSet);
$fwrite		=	fwrite($wh, $openProb);

$counter=0;

foreach (new SplFileObject($read) as $lineNumber => $lineContent) {


	$lCon=trim($lineContent);

	$pat='/Assumption,00$/';
	if(preg_match($pat, $lCon))
		$assumeIndex=strrpos($lCon, "Assumption,00");
	else
		$assumeIndex=false;


	if($counter===0){ 								//---------- conclude

		$s=substr($lCon, 0);

		$s=bicond($s);
//		$s=operators($s);

		$line="\t\t".'"c" : "'.extraParen($s).'" ,'."\r\n";
		$objC=$line;
		// { 

	}
	elseif($assumeIndex!==false){ 					//---------- assume

		$s=substr($lCon, 0, $assumeIndex-1);

		$s=bicond($s);
//		$s=operators($s);
		$s=extraParen($s);

		$line="\t\t".$s."\r\n";
		array_push($assume, $line);
	}
	elseif($lCon{0}==="*" && $lCon{1}!=="*"){ 		//---------- hint

		$s=substr($lCon, 1);
		$s=operators($s);
		$s=qToBicond($s);
		$s=negation($s);

		$line="\t\t".$s."\r\n";
		
		array_push($hint, $line);

	}
	else{ 											//---------- solve

		if($lCon !== '***'){

			$a=explode(",", $lCon);

			$st=trim($a[0]);
			$jt=trim($a[1]);

			$j=preg_replace('/(\d)\s(\d)/i', '$1,$2'  , $jt);

			$s=bicond($st);
			$j=bicond($j);
			$s=extraParen($s);
			$s=operators($s);
			$j=operators($j);
			$s=disjunction($s);
			$j=disjunction($j);

			$line="\t\t".$s.' '.$j."\r\n";

			array_push($solve, $line);
		}
	}

    if($lCon === '***') { 							//---------- next prob

 #   	$probCount+=1;

		fwrite($wh, $objC);

		$objA="\t\t".'"a" : {';
		$objS="\t\t".'"s" : {';
		$objH="\t\t".'"h" : {';

		foreach ($assume as $key => $value){

			$value=trim($value);

			if($key===count($assume)-1)
				$objA.='"'.$key.'" : "'.$value.'" ';
			else
				$objA.='"'.$key.'" : "'.$value.'", ';

		}
		$objA.='},'."\r\n";
		$fwrite=fwrite($wh, $objA);

		foreach ($solve as $key => $value){
			
			$value=trim($value);

			if($key===count($solve)-1)
				$objS.='"'.$key.'" : "'.$value.'" ';
			else
				$objS.='"'.$key.'" : "'.$value.'", ';

		}
		$objS.='},'."\r\n";
		$fwrite=fwrite($wh, $objS);

		foreach ($hint as $key => $value){
			
			$value=trim($value);

			if($key===count($hint)-1)
				$objH.='"'.$key.'" : "'.$value.'" ';
			else
				$objH.='"'.$key.'" : "'.$value.'", ';

		}
		$objH.='}'."\r\n";
		$fwrite=fwrite($wh, $objH);


		if($lineNumber < $lastLine-1){
			$fwrite=fwrite($wh, $closeProb);
			$fwrite=fwrite($wh, $openProb);
		}

		$assume=$solve=$hint=array();
    	$counter=0;
 		$line="";

    }
    else{
    	$counter+=1;
    }

	echo $line.'<br>';

}

$fwrite=fwrite($wh, $closeProbFin);
$fwrite=fwrite($wh, $closeSet);
fclose($rh);
fclose($wh);





function extraParen($s){

		$patL='/^\(/';
		$patR='/\)$/';

		if(preg_match($patL, $s) && preg_match($patR, $s)){

		$rep='';

		$a=preg_replace($patL, $rep, $s);
		$b=preg_replace($patR, $rep, $a);

		$line=$b;


		}
		else{
			$line=$s;
		}

		return $line;

}

function bicond($s){

	$s=preg_replace('/([A-Z]\)?)(<)/', '$1=', $s);
	$s=preg_replace('/<([IE])/', '=$1', $s);
	return $s;
}
function disjunction($s){
	$s=preg_replace('/v/', '&or;', $s);	
	return $s;
}
function qToBicond($s){

	$s=preg_replace('/([A-Z]\)?\s)(\?)(\s\??\(?[A-Z])/', '$1 &equiv; $3', $s);
	return $s;
}
function negation($s){

	$s=preg_replace('/\?/', '&sim;', $s);
	$s=preg_replace('/(&sim;)$/', '?', $s);
	return $s;


}
function operators($s){

//	$s=preg_replace('/&/', '*', $s);
	$s=preg_replace('/>/', '&sup;', $s);
	$s=preg_replace('/-/', '&sim;', $s);
	return $s;

}


?>