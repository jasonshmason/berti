/*
	bertie
		Logic functions for Rules Of Replacement
*/
/**
 * @author Jason Campbell
 * @email  jason@jasonscampbell.com
**/

/*
problem set
	problem
		assumptions
		conclusion




*/



(function(jQuery){
	jQuery.fn.extend({


		dem : function(a, c){	// deMorgans
		/*
		-(p & Q) === (-p v -q)
		-(p v q) === (-p & -q)
		*/

		// if assumption == &
		// 	if assumption == -													: <-(p & q)>
		// 		check symbol equivilance for this type
		// 	else if (assumption ant == -) && (assumption con == -)	: <-p & -q>
		// 		check symbol equivilance for this type

		// 	if conclusion == v && (conclusion ant == - && conclusion con == -)
		// 		if  (assumption ant == conclusion ant) && (assumption con == conclusion con)

		},
		idem : function(){ // idempotence
		/*
		p === (p & p)
		p === (p v p)
		*/
		},
		comm : function(){	// commutation
		/*
		(p v q) === (q v p)
		(p & q) === (q & p)
		*/

		},
		assoc : function(){	// association
		/*
		[p v (q v r)] === [(p v q) v r]
		[p & (q & r)] === [(p & q) & r]
		*/

		},
		dist : function(){	// distribution
		/*
		[p & (q v r)] === [(p & q) v (p & r)]
		[p v (q & r)] === [(p v q) & (p v r)]
		*/

		},
		dn : function(){	// double negation
		/*
		p === --p
		*/

		},
		trans : function(){	// transposition
		/*
		(p > q) === (-q > -p)
		*/

		},
		impl : function(){	// material implication
		/*
		(p > q) === (-p v q)
		*/

		},
		equiv : function(p){	// material equivilance
		/*
		(p === q) === [(p > q) & (q > p)]
		(p === q) === [(p & q) v (-p & -q)]
		*/


		},
		exp : function(){	// exportation
		/*
		[(p & q) > r] === [p > (q > r)]
		*/
		},
		taut : function(){	// tautology
		/*
		p === (p v q)
		p === (p & p)
		*/
		}


	});
})(jQuery);
