/* bertie

*/
/**
 * @author Jason Campbell
 * @email  jason@jasonscampbell.com
**/

/*

FUNCTION:		HTML:
conj			&sdot;
disj			&or;
cond			&sup;
bicond			&equiv;
therefore		&there4;
E-quant			&exist;
U-quant			&forall;
neg				&sim;
*/
(function(jQuery){

	jQuery.fn.extend({

		argument : {},
		assumptions : {},

		initBertie : function(){

			$.bertie=$(this);
			$.shift=false;
			$.ctrl=false;
			$.opt=false;
			$.aux=false;
			$.assumption={};
			$.parentAssumption;
			$.pSet;
			$.currentP=0;

			$.symbols={
				'v' : '&or;',
				'-' : '&sim;',
				'&' : '&sdot;',
				'>' : '&sup;',
				'=' : '&equiv;',
				'*' : '&forall;',
				'!' : '&exist;'
			}


			//entity for numeric reference character conversion
			$('body').append('<i style="position:absolute;top:-999px"></i>');

			return this.each( function(index) {

				var $this=$(this);	// div#interface

				$this.loadProblemSet();
				 $this.initLayout();
				$this.splashScreen();
				// $this.initMenu();
				// $this.find('input').initInput();


			});

		},
		initLayout : function(){

				var $this=$(this);	// div#interface

				$this.css({
					height	:	$(window).innerHeight()
				});
		},
		initInput : function(){

			var $this=$(this);

			$this.bind({

				keydown : function(e){

					if($(this).hasClass('blur')){
						$this.val('');
						$this.removeClass('blur');
					}
					 $('span.prompt').remove();

					$(this).convertInput(e);

				},
				click : function(e){

					if($(this).hasClass('blur')){
						$this.val('');
						$this.removeClass('blur');
					}
					 $('span.prompt').remove();

				},
				keyup : function(e){
					// clear shift and control key state
					e.which===16?
						$.shift=false
					:
						e.which===17?
							$.ctrl=false
						:
							'';

					// if($(this).attr('id')==='s')
					// 	$(this).val($(this).val().toUpperCase());
					// else{
					// 	!ruleFirst
					// 		ruleFirst=$(this).val().match(/[a-z]{1}$/);

					// }


				},
				blur : function(){
					$(this).toggleClass('blur');
				},
				focus : function(){

			//		$(this).closeSub();
					$('.menu-win').closeMenuWin();
					$('#control').closeMenu();

				}

			}).focus();

		},
		loadProblemSet : function(){
			// php reads json probs dir and returns file name list
			// create load set menu sub sub html
			// auto-load first in list

			var $this=$(this),	// div#interface
				prob;

			$this.error("Loading problem set", null);

			var jqxhr = $.getJSON("json/prob1.json", function(json) {

			})
			.success(function(json) {
			  jQuery.parseJSON(json);

			  	$.pSet=json.set;
				prob=json.set[$.currentP];


				$this.loadProblem(prob);
				$this.initLayout();
				$this.initMenu();
				$this.find('input').initInput();

			})
			.error(function(json) {
				$this.error("Problem set failed to Load<br/>Please refresh your browser and try again")
			 })
			.complete(function(json) {
				$('div#error').trigger('click');
				$this.removeClass('hide');

			});

		},
		loadProblem : function(problem){

			var $this=$(this), // div#interface
				iterator=1,
				alen = 0;

			// get number of assumptions
			for (k in problem.a) if (problem.a.hasOwnProperty(k)) alen++;

			// load primary assumptions
			$.each(problem.a, function(k, v){


				iterator===alen?
					aClass='a p l'
				:
					aClass='a p';

				$('ul#pri').appendLine(aClass, iterator, $(this).encode(v.split('')), 'Assumption');

				// append primary assumption statements to argument object
				$.bertie.argument[iterator]=v;
				iterator++;

			});

			// create first statement line
			$('ul#pri').appendLine('s', iterator, '<input data-line="'+iterator+'" id="s" class="blur" type="text"  />', null);

			// disable first statement input until splash screen closed
			if($('#splash').length)
			$('input#s').prop('disabled', true);
			// load target conclusion
			$('ul#con').appendLine('pr', '&there4;', $(this).encode(problem.c.split('')), '');

			// append conclusion statement to argument object
			$.bertie.argument['c']=problem.c;

			iterator=0;

			// load hints
			$this.hint(problem.h);

			$('#p-num span').text($.currentP+1);


		},
		appendLine : function(lineClass, lineNum, lineStatement, lineType){

			var $this=$(this),
				lineType=lineType===null?'':lineType;

			 numClass=lineClass==='s'?lineNum:'';

			if(lineStatement.indexOf('input')>-1)
				lineStatement='<span class="prompt">Enter a statement and a slash</span>'+lineStatement;



			var line=	"<li class='"+lineClass+" "+numClass+"'>"
					+	"<span>"+lineNum+"</span>"
					+	"<span class='s'>"+lineStatement+"</span>"
					+	"<span class='j'>"+lineType+"</span></li>";

			 $this.append(line);


		},
		encode : function(val){

			var $this=$(this),
				s='';

			// exchange each common html replacement symbol in statement with logic symbol for display
			for(var i in val){

				if(val[i] in $.symbols)
					altChar=$.symbols[val[i]];
				else
					altChar=val[i];

				s+=' '+altChar;

			 }

			 return s;
		},
		decode : function(val){

			var $this=$(this),	// <input id="s">
				s='';

			// create metasymbols object by transposing
			//	each numeric reference character  in $.statements object
			//  through DOM <i> object
			var metaSymbols={};

			$.each($.symbols, function(k, v){

					$('i').html(v);
					var key=$('i').text();

					metaSymbols[key]=k;

			});


			// exchange each logic symbol in statement for common html replacement
			for(var i in val){

				if(val[i] in metaSymbols)
					altChar=metaSymbols[val[i]];	// replace logic symbol with html
				else
					altChar=val[i].toUpperCase();	// set letters to uppercase

				s+=altChar;

			 }

 			 return s;

		},
		convertInput : function(event){

			var $this=$(this);	// extant input field

			var inputKind=$this.attr('id');

			var key=event.which;


			if(inputKind=='j'){// if <input> is justification field

				if(key==13) // 'enter'
					$this.dispatchJustification();

				return;

			}
			switch(key){
				case 13:		// enter
					inputKind=='j'?$this.dispatchJustification():
					inputKind=='s'?$this.dischargeAssumption(event):null;
					$('div#error').trigger('click');
					return
					break;
				case 191:		// slash
					inputKind=='s'?$this.dischargeStatement(event):'';
					return;
					break;
				case 16:		// shift
					$.shift=true;
					return;
					break;
				case 17:		// control
					$.ctrl=true;
					return;
					break;
				case 86:		// disj
					altChar='&or;';
					break;
				case 173:		// neg
					altChar='&sim;';
					break;
				case 190:		// conj / cond
					altChar=$.shift?'&sup;':'&sdot;';
					// $.shift=false;
					break;
				case 55:		// conj / cond
					altChar=$.shift?'&sdot;':null;
					// $.shift=false;
					break;
				case 61:		// bicond
					altChar='&equiv;';
					break;
				case 65:		// U-quant
					altChar=$.ctrl?'&forall;':null;
					// $.ctrl=false;
					break;
				case 69:		// E-quant
					altChar=$.ctrl?'&exist;':null;
					// $.ctrl=false;
				default:
					altChar=null;
					return;
					break;

			}

			if(altChar)
				event.preventDefault();

			$('i').html(altChar);
			c=$('i').html();
			var inputString=$this.val()+c;
			$this.val(inputString);

		},
		dischargeStatement : function(event){ // called when "/" entered at end of statement input

			event.preventDefault();

			var $this=$(this), // statement <input class="s">
				lineNum=$this.attr('data-line');



			// decode logic symbols to common symbols and add line to argument object
			x=$this.val().split('');
			$.bertie.argument[lineNum]=$this.decode(x);



			// if no input, do nothing
			if($this.val()==='')
				return;

			$this.parent().next().append('<span class="prompt">Enter justification</span><input id="j" class="blur" type="text" />');
			$('#j').initInput();

			// setup statement <input> parent for editing, replace statement <input> with it's value string
			$this.parent().addClass('open')
							.data('line', lineNum)
							.click(function(){ // statements can be edited until their justification returns true
								$(this).editLine();
							});

			$this.replaceWith($this.val());

		},
		dispatchJustification : function(){ // called when "enter" pressed at end of justification input

			var $this=$(this),	// <input id="j">
				proof=constructProof(); // construct object of current line, sited lined, an sited rule


			discharge=function(QED){ // callback from rule function


				if(proof.r.length>2){ // prettify rule abbreviation

					rArr=proof.r.split('');
					rArr[0]=rArr[0].toUpperCase();
					rStr=rArr.join('');

				}
				else
					rStr=proof.r.toUpperCase();


				var targ,
					pretty=proof.l2?
								proof.l1+', '+proof.l2+' '+rStr
							:
								proof.l1+' '+rStr;

				$this.replaceWith(pretty);	// replace <input id="j"> with its prettified value

				pretty=$('.open').text().split('').join(' '); // prettify last statement
				$('.open').text(pretty);


				$('.open').toggleClass('open').unbind('click'); // unbind statement span from click

				if(QED){	// if last statement matches thesis
					$this.error('Quod Erat Demonstrandum');
				}
				else{

					newLine=proof.tl+1; // next line num (last input line num + 1)

					targ=$('li.'+proof.tl).closest('ul');

					if(proof.r==='a'){ // discharged line is head of auxiliary assumption
						$.aux=true;// current proof remains an auxiliary assumption until dischargeAssumption()

						targ=$('ul.aux:last'); // new line target is auxiliary assumption

					}
					if($.aux){

targ.data('l').push(proof.tl); // add line number to assumptions lines array
						$.assumption[proof.tl]=$.bertie.argument[proof.tl];

					}

					targ.appendLine('s', newLine, '<input data-line="'+newLine+'" id="s" class="blur" type="text" />', null);

					$('#interface').find('input').initInput();
					$('#con').checkProofHeight();
				}


			}

			// try{

				var err, rule;

				switch(true){			// swap bertie3 rules for copi
					case proof.r==='>e':
						r='mp';
						err='Modus Ponens (MP)';
					break;
					case proof.r==='>i':
						r='cp';
						err='Conditional Proof (CP)';
					break;
					case proof.r==='&e':
						r='simpl';
						err='Simplification (Simpl)';
					break;
					case proof.r==='&i':
						r='add';
						err='Addition (Add)';
					break;
					case proof.r==='-i':
						r='ip';
						err='Indirect Proof (ID)';
					break;
					case proof.r==='-e':
						r='ip';
						err='Indirect Proof (ID)';
					break;
					case proof.r==='=i':
						r='bi';
						err='Biconditional Introduction (BI)';
					break;
					default:
						r=proof.r;
					break;
				}

				errtext="<em>"+err+"</em>";
				if(err){
					proof.r=r;
					$this.error(errtext, 1);
				}

				$this[r](proof); // send line proof obj to function of sited rule
			// }
			// catch(err){
			// 	console.log(err.message);
			// }


			function constructProof(){

				//------------------------------------------------------------ parse rule name and line numbers from citation
				try{

					var citation=$this.val().toLowerCase().replace(/\./g, ''),	// set citation to lc and strip any periods
						rule=citation.match(/>?&?[a-z]+$/).toString(), // all letters at eol
						linesSeg=citation.substr(0,citation.indexOf(rule)), // sited lines string
						lines=linesSeg.split(',');	//single line or single range : lines.length=1,  multiple lines or multiple ranges : lines.length=2

				}catch(err){

					$this.error('malformed justification');
					throw '';

				}


				//------------------------------------------------------------ get last input statement (proof thesis) line number from argument obj
				// var keys=[];
				// $.each($.bertie.argument, function(k, v){
				// 	if(!isNaN(k))keys.push(k);
				// });
				var tl=thesisLine(); // entered statement = highest numbered item in argument obj



				//------------------------------------------------------------ create and return object of parsed data
				var pObj={
					r	:	rule,
					tl	:	tl,
					l1	:	lines[0],
					l2	:	lines[1]
				}

				return pObj;
			}

			function thesisLine(){

				//------------------------------------------------------------ get last input statement (proof thesis) line number from argument obj
				var keys=[];
				$.each($.bertie.argument, function(k, v){
					if(!isNaN(k))keys.push(k);
				});
				return Math.max.apply( Math,keys); // entered statement = highest numbered item in argument obj

			}

		},
		drawAssumption : function(p){ // called from inferenceRules function "a" when justification is "a" from dispatchJustification

			// here, attach data obj to each aux and, in dispatchJustification, append line num to "this.closest .aux.data"
			// that way parent aux assumptions.data will have only their own line nums and
			// not the line nums of their children aux

			// when aux assumptions discharged add "closed" class to that aux head UL then,
			// with each dispatchJustification, check ea. .aux.closed.data for sited line nums
			// if present, throw error, else continue



			var $this=$(this),	// parent <li>
				arg=$.bertie.argument,
				s='',
				tl=p.tl;
											//parse statements into constituents: left operand, right operand, operator

			var l=$this.atomize(arg[tl]);

				op=l.op?l.op:'';
				ro=l.ro?l.ro:'';

				s+=l.lo+' '+op+' '+ro; // concat statement
				s=$this.encode(s);		// encode operators


			var parAux=$this.parents('ul.aux').length,
				allAux=$('ul.aux').length,
				idNum=parAux+1;

			var auxID=	$('#aux'+idNum).length?
							'aux'+idNum//+'_'+($('#aux'+idNum).length+1)
						:
							'aux'+idNum;
			// copy 3 spans to var
			// replace parent li with auxNode
			var auxNode=	'<li>'
						+		'<ul class="aux aux_'+idNum+'" id="'+auxID+'">'
						+		'<li class="marginline"><span></span></li>'
						+		'</ul>'
						+	'</li>';


			var auxLine=	'<li class="a l '+p.tl+'">'
						+	'<span>'+$this.children(':first').html()+'</span>'	// append 3 spans here
						+	'<span class="s">'+s+'</span>'
						+	'<span class="j">Assumption</span>'
						+	'</li>';

			// append auxLine to  last ul.aux
			$this.replaceWith(auxNode);
			$('.aux:last').append(auxLine)
.data('l',[]);	// add empty data array to this assumption to store its line numbers @
											// dispatchJustification -> discharge()

			delete s;


		},
		dischargeAssumption : function(event){ // called fron initInput when input class='s' and event.which is "enter"

			event.preventDefault();

			var $this=$(this);

$this.closest('.aux').addClass('closed');

							// count keys in assumptions object
			var keys=[];
			$.each($.assumption, function(k, v){
				if(!isNaN(k))keys.push(k);
			});

			keyCnt=Math.min.apply( Math,keys); // assumptions parameter = line number of assumption proof



			auxParNum=$this.parents('.aux').length>0? 				// are we in a nest of auxiliary assumptions?
				$this.parents('.aux').length-1
			:
				0;

			if(auxParNum===0){ // if not,
				$.aux=false;	// turn off assumption and
			}

			$.bertie.assumptions[keyCnt]=$.assumption;	// append completed assumption to assumptions object and
			$.assumption={}; // clear assumption object for additional assumptions



			$('i').html('');


			auxPar=auxParNum? // if parent is auxiliary, discharge to parent
				'ul#aux'+auxParNum
			:
				'ul#pri'; // else, discharge to primary

			$(this).closest('li').appendTo($this.closest(auxPar));



		//	$('ul#pri').find('input#s').addClass('blur').val('Enter a statement and a slash').focus();
			$('ul#pri input#s').before('<span class="prompt">Enter a statement and a slash</span>').focus();
		},
		inAssumption : function(p){ // if sited line(s) from previous auxiliary assumption : error

			var $this=$(this),
				pLines=[],
				truth,
				pat=/l[0-9]/;


			$.each(p,function(k,v){

				if(pat.test(k) && v)	// if key is a sited line and its value exists
					pLines.push(parseInt(v));

			});


			$('.aux.closed').each(function(){	// each closed aux assumption

				var aLines=$(this).data('l');
				for(var j in aLines){	// each closed aux assumption data array
					for(var i in pLines){


						if(pLines[i]===aLines[j]){
							truth=true;
							break;
						}

					}
				}
			});

			return truth;
		},
		editLine : function(){

			var $this=$(this);

			//replace input j with its val
			$('input#j').remove();
			sIn='<input data-line="'+$this.data('line')+'" id="s" type="text" />';

			$this.toggleClass('open')
				.unbind('click')
				.html(sIn);

				$('#interface input').initInput();

			// on unclosed statement click
				// replace <p> with statement <input>
				// <input>.val()=<p>.text()
				// remove justification <input>
		},
		hint : function(h){

			var $this=$(this);

			$('#hint').remove();

			var hints='',
				len,
				show=-1;

			$.each(h, function(k, v){

				n=parseInt(k)+1;

				hints+='<P id="'+k+'"><span>'+n+'.</span><span>'+v+'.</span></p>';

				len=k;

			});

			$this.append('<div id="hint">'+hints+'</div>');

			$('#hint').click(function(){

				show<len?show++:show=0;

				$(this).children().hide();
				$(this).children('#'+show).show();

			});

		},
		error : function(text, fade){

			var $this=$(this);

			var errDiv='<div id="error"><h3>!</h3><p>'+text+'</p><span>close</span></div>';

			if($(errDiv))$("div#error").remove();

			$('body').append(errDiv);

			var err=$('div#error'),
				errL=err.width()/2*-1,
				errT=err.height()/2*-1;
			err.css({
				marginTop	:	errT,
				marginLeft	:	errL
			})

			if(fade){
				setTimeout(function(){
					err.fadeOut(500, function(){
						err.remove();
					});
				},2000);
			}

			err.click(function(){
				err.remove();
				$('input').focus();
			});

		},
		checkProofHeight : function(){

			proofHeight=$(this).outerHeight() + $(this).offset().top;
			winHeight=$(window).innerHeight();

			proofHeight>=winHeight?
				$('#interface').css({overflowY : 'scroll'})
			:
				$('#interface').css({overflowY : 'hidden'});



		}


	});


})(jQuery);
$(document).ready(function(){
	$('div#interface').initBertie();
});