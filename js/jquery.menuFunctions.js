/* bertie interface  */
/**
 * @author Jason Campbell
 * @email  jason@jasonscampbell.com
**/


/*
BErtie 3 menu

File:
	load
	save
	print
	quit
Problem:
	get next
	pro <n>
	new
Edit:
	redraw
	insert
	delete
	discharge
help:
	topics
hints:
	hint
	lines <n>
	uncle
Rules:
	select set
	show forms
	discharge
*/

/*
Problem:
	load set ctrl+l
	get next ctrl+n
	pro <n>	 ctrl+p
	new
hints:
	hint ctrl+h
	uncle
Rules:
	select set
	show forms

*/
(function(jQuery){

	jQuery.fn.extend({

		boxShadow : '2px 2px 2px 0 #666',
		symbols : {
			'.'	: 	'&sdot;',
			'>'	: 	'&sup;',
			'v'	: 	'&or;',
			'A'	: 	'&forall;',
			'E'	: 	'&exist;',
			'-'	: 	'&sim;'
		},

		initMenu : function(){

			var $this=$(this); // div#interface

			$.activate;
			$.callback;

			var ua=navigator.userAgent;

			$.plat=	ua.indexOf('Mac')>-1?'mac':
						ua.indexOf('Windows')>-1?'win':
							'mobile';

			$.browser=	ua.indexOf('Gecko')>-1?'gecko':
							ua.indexOf('AppleWebKit')>-1?'webkit':
										ua.indexOf('IE')>-1?retardedBrowserVer():
											'other';

			function retardedBrowserVer(){

				//append version num to 'retard'
				return 'retard';
			}

			$(window).initKeyCommands();

			$('#control').bind({
				click : function(event){

					event.stopPropagation();

					var menu=$(this);

					parseInt(menu.css('top'))<0?
						menu.openMenu()
					:
						menu.closeMenu();

				}
			});

			$('.menuitem').bind({
				click : function(event){

					event.stopPropagation();

					$('#error').remove();

					var targFunc=$(this).attr('id'); // menu button id = function name

					$(this).closeMenuWin();

			//		if($('.'+targFunc).length===0) // if target window is not extant, call its function
						$(this)[targFunc]();
				}
			});

			$('#window').bind({
				click : function(event){
					event.stopPropagation();
					$('.menu-win').closeMenuWin();

					$('#control').closeMenu();
				}
			});

			$('.menuitem ul').each(function(){

				$(this).data('h', $(this).outerHeight());

			});
		},
		keymap : function(){

			var $this=$(this);

			$this.closeSub();

			$.activate=function(){ // callback from menuHTML function

				$('#keys').openMenuWin();


				$('#keys li').bind({
					mouseenter : function(){

					},
					click : function(event){ // keyboard key highlighting

						$('#keys li').buttonAnim(event, $(this));

						var keyChar=$(this).children('p');
						$('.frame').remove();
						keyChar.each(function(k,v){

							var keyFrame='<div class="frame '+k+'"></div>';
							$('#keyboard').append(keyFrame);

							framePos($(this).text(), k);

						});


					}
				});

				$.activate='';

			}

			var bgImg=$.plat==='mac'?'img/key-mac.png':'img/key-pc.gif'; // platform specific keyboard image

			$this.menuWin('keys', bgImg); // draw window html in menuHTML.js

			function framePos(key, c){ // coordinates for key highlighting


				var mac=$.plat==='mac',
					win=$.plat=='win',
					w=40;

				switch(key){

					case 'return'	: if(mac){l=536;t=128;w=73;}else if(win){l=520;t=153;w=89;}else '';
						break;
					case 'shift'	: if(mac){l=14;t=168;w=91;}else if(win){l=9;t=192;w=91;}else '';break;
					case 'ctrl'		: if(mac){l=55;t=211;}else if(win){l=9;t=233;w=48;}else '';break;
					case '.'		: if(mac){l=435;t=168;}else if(win){l=422;t=193;}else '';break;
					case '>'		: if(mac){l=435;t=168;}else if(win){l=422;t=193;}else '';break;
					case 'v'		: if(mac){l=229;t=168;}else if(win){l=221;t=193;}else '';break;
					case 'A'		: if(mac){l=86;t=129;}else if(win){l=77;t=153;}else '';break;
					case 'E'		: if(mac){l=158;t=88;}else if(win){l=148;t=113;}else '';break;
					case '='		: if(mac){l=507;t=48;}else if(win){l=490;t=73;}else '';break;
					case '-'		: if(mac){l=466;t=48;}else if(win){l=9;t=73;}else '';break;
					case '/'		: if(mac){l=475;t=168;}else if(win){l=462;t=193;}else '';break;

				}

				$('.'+c).css({
					top		:	t,
					left	:	l,
					width	:	w
				});

				operator(key);

			}

			function operator(key){ // logic operator info text

				var name,archetype,eng;

				switch(key){

					case 'return'	: null;break;
					case 'shift'	: null;break;
					case 'ctrl'		: null;break;
					case '/'		: null;break;
					case '.'		:
						name='<h1>Conjunction</h1>'
						archetype='<h2>P &sdot; Q</h2>';
						eng='<h3>"P and Q"</h3>';
						break;
					case '>'		:
						name='<h1>Conditional</h1>'
						archetype='<h2>P &sup; Q</h2>';
						eng='<h3>"P implies Q"</h3>';
						break;

					case 'v'		:
						name='<h1>Disjunction</h1>'
						archetype='<h2>P &or; Q</h2>';
						eng='<h3>"P or Q"</h3>';
						break;

					case 'A'		:
						name='<h1>Universal Quantifier</h1>'
						archetype='<h2>&forall;x</h2>';
						eng='<h3>"For every x"</h3>';
						break;

					case 'E'		:
						name='<h1>Existential Quantifier</h1>'
						archetype='<h2>&exist;x</h2>';
						eng='<h3>"There is an x"</h3>';
						break;

					case '='		:
						name='<h1>Biconditional</h1>'
						archetype='<h2>P &equiv; Q</h2>';
						eng='<h3>"P if and only if Q"</h3>';
						break;

					case '-'		:
						name='<h1>Negation</h1>'
						archetype='<h2>&sim;P</h2>';
						eng='<h3>"not P"</h3>';
						break;


				}

				name?
					$('#operators').html(name+archetype+eng)
				:
					$('#operators').html('');

			}

		},
		viewrules : function(){

			var $this=$(this);

			$this.closeSub();

			$.activate=function(){ // callback from menuHTML function

				$('#rules').openMenuWin();

				$('#btn.menu li').bind({
					click : function(event){
						$('.menu li').buttonAnim(event, $(this));

						var index=$(this).index();
						var slide=$('.slide').eq(index);
						var left=slide.position().left*-1;

						$('ul#slider').menuAnim({left:left});

					}
				});

			$.activate='';
			}

			$this.menuWin('rules');

		},
		convert : function(){

			var $this=$(this);

			$this.closeSub();

			$.activate=function(){ // callback from menuHTML function

				var $this=$('#converter');

				$this.openMenuWin();

				$this.find('#berfile').click(function(event){

					event.stopPropagation();

					$this.berfile();

				});
				$this.find('#fileinput, #filebutton').click(function(event){

					event.stopPropagation();
					$('input#BER').trigger('click')
								.change(function(){
									$('#fileinput').val($(this).val());

								});

				});

				$.activate='';

			 }

			$this.menuWin('converter');

		},
		problem : function(){

			var $this=$(this),
				pSetLen=$.pSet.length;

			$this.openSub();

// if set loaded, blur : focus

			$('#loadnumber input')
			.focus()
			.bind({

				keydown : function(event){

					var $this=$(this),
						key=event.which,
						pressed=String.fromCharCode(key),
						numKey=pressed.match(/[0-9]/),
						bs=key===8,
						ret=key===13,
						del=key===46;

					if(!bs && !ret && !del && !numKey)
						event.preventDefault();


				},
				keyup : function(event){

					 var $this=$(this),
					 	key=event.which,
					 	probVal=$this.val();

					 if(probVal>pSetLen){
					 	probVal=$this.val(pSetLen);
					 }
					 if(probVal<1){
					 	probVal=$this.val('');
					 }


					 if(key===13 && probVal.length){
					 	$this.loadnumber();
					 	$this.blur();
					 }

				}
			});

		},
		loadset : function(){
			$(this).error('Not yet functioning');


		},
		loadnext : function(){

			$.currentP+=1;
			$("#interface").loadExecute($.currentP);
			$(this).closeSub();
			$('#control').closeMenu();
		},
		reload : function(){

			$("#interface").loadExecute($.currentP);
			$(this).closeSub();
			$('#control').closeMenu();

		},
		loadnumber : function(){

			var $this;

			$(this).attr('id')==='loadnumber'?
				$this=$(this).find('#loadpnum')
			:
				$this=$(this);

			if($this.val()==='')
				return;

			$.currentP=$this.val()-1;

			$("#interface").loadExecute($.currentP);
			$this.closeSub();
			$('#control').closeMenu();
			$this.val('');
		},
		loadExecute : function(n){

			var $this=$(this);

			// clear objects
			$.bertie.assumptions={};
			$.bertie.argument={};
			$.assumption={};

			$('#pri .a').remove();
			$('#pri .s').remove();
			$('ul#con .pr').remove();

			p=$.pSet[n]
			$(this).loadProblem(p);

			$this.initLayout();
			$this.find('input').initInput();

		},
		gethint : function(){

		$('div#hint').show().trigger('click');
		$(this).closeSub();

		},
		closeHint : function(){

			$('div#hint').hide();
//			$('div#interface').hint(problem.h);

		},
		save : function(){
			//
			$(this).error('Not yet functioning');
			$(this).closeSub();

		},
		options : function () {

			var $this=$(this);

			$(this).closeSub();
				$this.openSub();

		},
		openMenu : function(t){

			var $this=$(this);

			$this.addClass('down')
				.menuAnim({top:0});

			$.callback=function(el, st, en){
				return true;
			}
		},
		closeMenu : function(t){

			var $this=$(this);

			$(this).closeSub();
			$(this).closeHint();

			$this.removeClass('down')
				.menuAnim({top:($this.height()+2)*-1});
		},
		openMenuWin : function(){

						$(this).menuAnim({top:'50%'});

		},
		closeMenuWin : function(){

			if($('.menu-win').length)
				$('.menu-win').menuAnim({top:'-40%'}); // slide up any extant windows
		},
		openSub : function(){

			var $this=$(this),
				sub=$this.find('ul'),
				fin=sub.data('h');

			sub.css({height : 0, borderColor : '#000', display : 'block'})
			sub.menuAnim({'height' : fin}, 1000);
			sub.addClass('opensub').find('input').prop('disabled', false).focus(); // activate any submenu inputs;

		},
		closeSub : function(){

			var $this=$(this),
				fin=0,
				sub=$('.opensub');

			if(sub.length){
				sub.menuAnim({'height' : fin}, 600);
				sub.find('input').blur().prop('disabled',true);	// ensure menu input is blurred
			}

			$.callback=function(el, st, en){
				sub.toggleClass('opensub')
					.css({display : 'none', height : 'auto', borderColor : 'transparent'});
				$.callback='';
			}

		},
		menuAnim : function(prop, dur){

			var $this=$(this),
				dur=dur?dur:600;

			$this.animate(
				prop,
				{
					duration : dur,
					easing : 'easeOutExpo',
					step : function(c,f){

						p=f.prop;
						el=f.elem;
						st=f.start;
						en=f.end;

					},
					complete : function(){

						if($(this).hasClass('menu-win') && parseInt(prop[p])<0)
							$(this).remove();

						try{return $.callback(el, st, en);}catch(err){}

					}
				}
			);

		//	try{return $.activate();}catch(err){}
		},
		buttonAnim : function(event, clicked){

			var $this=$(this);

			event.stopPropagation();
			$this.each(function(){

				$(this).css({boxShadow : $this.boxShadow});
			});

			clicked.css({boxShadow:'0 0 0 0'});

		},
		initKeyCommands : function(){

			var $this=$(this),
				altKey,lbl,ltr;

			if($.plat==='mac'){
				altKey=$.ctrl;
				lbl="ctrl";
			}
			else if($.plat==='win'){
				altKey=$.opt;
				lbl="alt";
			}
			else{}

			$("#control small").each(function(){

				ltr=$(this).text();
				$(this).text(lbl+" "+ltr);

			});

			$this.bind({
				keydown : function(event){

					var key=event.which;

					$.plat==='mac'?
						keyNum=17
					:
						$.plat==='win'?
							keyNum=18
						:
							'';

					switch(key){
						case keyNum:		// key==keyNum?
							altKey=true;
							return;
							break;
						case 78:		//ctrl + n
//							event.preventDefault();
							altKey?$this.loadnext():null;
							break;
						case 80:		//ctrl + p
//							event.preventDefault();
							if(altKey){
								if($('#control').openMenu());
								$('li.menuitem#problem').problem();
							}
							break;
						case 82:		//ctrl + r
//							event.preventDefault();
							altKey?$this.reload():null;
							break;
						case 72:		//ctrl + h
							if(altKey){
								event.preventDefault();
								$this.gethint();
							}
							break;
						default:
							return;
							break;

					}



				},
				keyup : function(event){

					var key=event.which;

					// console.log(key===17 || key===18);
					// if(key===17 || key===18)
						altKey=false;
				}
			});

		},
		splashScreen : function(){


			var $this=$(this),
				winH=$(window).innerHeight(),
				winW=$(window).innerWidth();


			if($this.splash()){

				var splash=$('#splash'),
					splashW=winW/2,
					splashH=winH/2,
					splashT=splashH/2,
					splashL=splashW/2;

				splash.css({
					width	:	splashW,
					height	:	splashH,
					top		:	splashT,
					left	:	splashL
				});

				$('#window, #splash').bind({
					click : function(){
						$('#window').removeClass('splashBG');
						$('#splash').remove();
						$('input#s').prop('disabled', false).focus();
						$(window).unbind('keypress');
					}
				});

				$(window).bind({

					keypress : function(){
						$('#window').removeClass('splashBG');
						$('#splash').remove();
						$('input#s').prop('disabled', false).focus();
						$(window).unbind('keypress');
					}

				});




			}

		}

	});


})(jQuery);
