(function(jQuery){

	jQuery.fn.extend({

		menuWin : function(item, param){

			var $this=$(this),
				win;

			win='<div id="'+item+'" class="menu-win '+$this.attr('id')+'"></div>';

			$('body').append(win);

				$('.menu-win').bind({
					click : function(event){
						event.stopPropagation();
						$(this).menuAnim({top:'-40%'});

						$('#control.down').closeMenu();
					}
				});

			$('#'+item)[item](param);

		},
		rules : function(){

			var $this=$(this),
				slide,
				menu='',
				rules={
					MT		:	{n : 'Modus Tollens',
									a : {l : {1:'p &sup; q', 2:'&sim;q', 3:'&there4; &sim;p'},
										e : {1:'If p implies q', 2:'and q is false', 3:'then p is false'}}
									},
					HS		:	{n : 'Hypothetical Syllogism',
									a : {l : {1:'p &sup; q', 2:'q &sup; r', 3:'&there4; p &sup; r'},
										e : {1:'If p implies q', 2:'and q implies r', 3:'then p implies r'}}
									},
					DS		:	{n : 'Disjunctive Syllogism',
									a : {l : {1:'p &or; q', 2:'&sim;p', 3:'&there4; q'},
										e : {1:'If p or q is true', 2:'and p is false', 3:'then q is true'}
									},
									b : {l : {1:'p &or; q', 2:'&sim;q', 3:'&there4; p'},
										e : {1:'If p or q is true', 2:'and q is false', 3:'then p is true'}}
									},
					// Dem		:	{n : 'DeMorgan',
					// 				a : {l : {1:'&sim;(p &sdot; q) &equiv; &sim;p &or; &sim;q', 2:'&sim;(p &or; q)  &equiv;  &sim;p &sdot; &sim;q'},
					// 					e : {1:'p and q is false if and only if p is false or q is false', 2:'p or q is false if and only if p is false and q is false'}}
					// 				},
					Add		:	{n : 'Addition',
									a : {l : {1:'p', 2:'&there4; p &or; q'},
										e : {1:'If p is true', 2:'then p or q is true'}}
									},
					Conj	:	{n : 'Conjunction',
									a : {l : {1:'p', 2:'q', 3:'&there4; p &sdot; q'},
										e : {1:'If p is true', 2:'and q is true', 3:'then p and q is true'}}
									},
					Simpl	:	{n : 'Simplification',
									a : {l : {1:'p &sdot; q', 2:'&there4; p'},
										e : {1:'If p and q is true', 2:'then p is true'}
									},
									b : {l : {1:'p &sdot; q', 2:'&there4; q'},
										e : {1:'If p and q is true', 2:'then q is true'}}
									},
					DD		:	{n : 'Destructive Dilemma',
									a : {l : {1:'(p &sup; q) &sdot; (r &sup; s)', 2:'&sim;q &or; &sim;s', 3:'&there4; &sim;p &or; &sim;r'},
										e : {1:'If p implies q and r implies s is true', 2:'and q is false or s is false', 3:'then p is false or r is false'}}
									},
					CD		:	{n : 'Constructive Dilemma',
									a : {l : {1:'(p &sup; q) &sdot; (r &sup; s)', 2:'p &or; r', 3:'&there4; q &or; s'},
										e : {1:'If p implies q and r implies s is true', 2:'and p or r is true', 3:'then q or s is true'}}
									},
					CP		:	{n : 'Conditional Proof',
									a : {l : {1:'p', 2:'q', 3:'&there4; p &sup; q'},
										e : {1:'If p follows from q', 2:'then p implies q'}}
									},
					DN		:	{n : 'Double Negation',
									a : {l : {1:'p', 2:'&there4; &sim;&sim;p'},
										e : {1:'If p is true', 2:'then not p is false'}
									},
									b : {l : {1:'&sim;&sim;p', 2:'&there4; p'},
										e : {1:'If not p is false', 2:'then p is true'}}
									},
					IP		:	{n : 'Indirect Proof',
									a : {l : {1:'p', 2:'&sim;p', 3:'&there4;q'},
										e : {1:'If p is true', 2:'and p is false', 3:'then q is true'}}
									},
					R		:	{n : 'Reiteration',
									a : {l : {1:'p', 2:'&there4;p'},
										e : {1:'If p is true', 2:'then p is true'}}
									}
				};

			$this.append('<ul id="slider"></ul>');
			$this.append('<ul id="btn" class="menu"></ul>');

			$.each(rules, function(k, v){

				slide	=	'<li class="slide">'
						+		'<h1>'+v.n+' ('+k+')</h1>'
						+		'<div class="content">'
						+			'<h2>Archetype</h2>'
						+			'<h2>English</h2>'
						+			'<div class="archetype">'
						+				'<ul class="archetype">';

				 $.each(v.a.l, function(key, val){
					slide	+=	'<li>'+val+'</li>';
				 });

				 slide	+=	'</ul><ul class="eng">';

				 $.each(v.a.e, function(key, val){
					slide	+=	'<li>"'+val+'"</li>';
				 });

				if(v.b){

					slide	+=			'</ul>'
							+		'</div><hr/>'
							+		'<div class="archetype">'
							+			'<ul class="archetype">';


					 $.each(v.b.l, function(key, val){
						slide	+=	'<li>'+val+'</li>';
					 });

					 slide	+=	'</ul><ul class="eng">';

					 $.each(v.b.e, function(key, val){
						slide	+=	'<li>'+val+'</li>';
					 });

				}

				slide	+=	'</ul></div></div></li>';

				menu	+=	'<li><span>'+k+'</span></li>';

				$('ul#slider')	.append(slide)
								.width($('li.slide').length*$('div#rules').width());

			});

			$('#btn').append(menu);

			$('.slide').css({width:$('#rules').width()});

			$('.content').each(function(){
				h=$(this).height();
				$(this).css({top:'50%', marginTop:(h/2)*-1});
			});

			return $.activate();

		},
		keys : function(bgImg){

			var win=		'<h1>Operators</h1>'
					+		'<ul class="menu">'
					+			'<li><span>&sdot;</span><p>.</p></li>'
					+			'<li><span>&or;</span><p>v</p></li>'
					+			'<li><span>&sup;</span><p>shift</p> + <p>></p></li>'
					+			'<li><span>&equiv;</span><p>=</p></li>'
					+			'<li><span>&forall;</span><p>ctrl</p> + <p>A</p></li>'
					+			'<li><span>&exist;</span><p>ctrl</p> + <p>E</p></li>'
					+			'<li><span>&sim;</span><p>-</p></li>'
					+		'</ul>'
					+		'<h1>Actions</h1>'
					+		'<ul class="menu" id="actions-list">'
					+			'<li id="dischargeStatement"><span>discharge statement</span><p>/</p></li>'
					+			'<li id="dischargeJustification"><span>discharge justification</span><p>return</p></li>'
					+			'<li><span>discharge assumption</span><p>return</p></li>'
					+		'</ul>'
					+		'<div id="operators"></div>'
					+		'<div id="keyboard"><img src='+bgImg+' /></div>';


			$(this).append(win);
			return $.activate();

		},
		splash : function(){

			var $this=$(this),
				splash 	=  '<div id="splash">'
						+	'<div class="splashBG"></div>'
						+	'<h1>Berti</h1>'
						+	'<p>A proof checker for natural deduction systems in'
						+	' sentential and predicate logic</p>'
						+	'<p>&copy; 2013 By Jason Campbell<br />GNU General Public License</p>'
						+	'<p>A replication of Bertie3 By Austen G. Clark</p>'
						+	'<span>Touch any key to begin...<em>_</em></span>'
						+	'</div>';


			$('#window').addClass('splashBG');

			$('body').append(splash);

			return true;
		},
		converter : function(){

			var win=		'<h1>Bertie3 Problem Files</h1>'
					+		'<div class="content" >'
					+		'<p>Your custom Bertie3 problem files ( <em>filename</em>.ber ) can be converted for use in Berti.</p>'
					+		'<p>In order to successfully convert the Bertie 3 problem file must adhere to the Bertie 3'
					+		' problem file syntax with the final3 asterisk delimiter line deleted.</p>'
					+		'<ul class="menu"><li id="berfile"><p>BER file syntax sample</p></li></ul>'
					+		'<p>The four problem files included with Bertie 3 are already converted and ready for use.</p>'
					+		'</div>'
					+		'<div class="content" >'
					+		'<p>Please use input box below to upload your BER file to the converter.</p>'
					+		'<div id="file"><input type="text" id="fileinput" length="30" /><span id="filebutton">select</span></div>'
					+		'<form name="fileupload"><input type="file" name="BER" id="BER" length="30" /></form>'
					+		'</div>';


			$(this).append(win);
			$('input#BER').click(function(event){event.stopPropagation();});
			return $.activate();

		},
		berfile : function(){

			var $this=$(this);

			var file=	'<div id="dotber">'
					+	'<ul id="detail">'
					+	'<li class="conclude">'
					+	'<em>conclusion</em><span></span>'
					+	'</li>'
					+	'<li class="assume">'
					+	'<em>primary assumptions</em><span></span>'
					+	'</li>'
					+	'<li class="solve">'
					+	'<em>solve (uncle)</em><span></span>'
					+	'</li>'
					+	'<li class="hint">'
					+	'<em>hints</em><span></span>'
					+	'</li>'
					+	'<li class="penult">'
					+	'<em>first through penultimate proofs</em><span></span>'
					+	'</li>'
					+	'<li class="last">'
					+	'<em>last proof:<br />please delete last line of asterisks</em><span></span>'
					+	'</li>'
					+	'</ul>'

					+	'<pre>'
					+	'<h1>PROBS1.BER</h1>'
					+	'<span>'
					+	'(L>A)\n'
					+	'(L>J),Assumption,00\n'
					+	'(J>A),Assumption,00\n'
					+	'L,Assumption,0003\n'
					+	'J,1 3 >E,0003\n'
					+	'A,2 4 >E,0003\n'
					+	'(L>A),3-5 >I,00\n'
					+	'*The conclusion is a conditional\n'
					+	'*Use conditional proof\n'
					+	'*Start by assuming L\n'
					+	'***\n'
					+	'</span>'
					+	'<span>'
					+	'(L>A)\n'
					+	'(L>J),Assumption,00\n'
					+	'(J>A),Assumption,00\n'
					+	'L,Assumption,0003\n'
					+	'J,1 3 >E,0003\n'
					+	'A,2 4 >E,0003\n'
					+	'(L>A),3-5 >I,00\n'
					+	'*The conclusion is a conditional\n'
					+	'*Use conditional proof\n'
					+	'*Start by assuming L\n'
					+	'***\n'
					+	'</span>'
					+	'<span>'
					+	'(L>A)\n'
					+	'(L>J),Assumption,00\n'
					+	'(J>A),Assumption,00\n'
					+	'L,Assumption,0003\n'
					+	'J,1 3 >E,0003\n'
					+	'A,2 4 >E,0003\n'
					+	'(L>A),3-5 >I,00\n'
					+	'*The conclusion is a conditional\n'
					+	'*Use conditional proof\n'
					+	'*Start by assuming L\n'
					+	'<p class="red">***</p>\n'
					+	'</span>'
					+	'</pre>'
					+ 	'</div>';

			$this.append(file);
			$('#dotber').click(function(event){
				event.stopPropagation();
				$(this).remove();
			});


		}


		// ,
		// menu : function(){

		// 	var win=	'<div id="control">'
		// 					'<ul class="menu">'
		// 						'<li class="menuitem" id="loadset">load problem set</li>'
		// 						'<li class="menuitem" id="loadnext">next problem</li>'
		// 						'<li class="menuitem" id="reload">reload problem</li>'
		// 						'<li class="menuitem" id="gethint">get hint</li>'
		// 						'<li class="menuitem" id="viewrules">view rules</li>'
		// 						'<li class="menuitem" id="keymap">key map</li>'
		// 						'<li class="menuitem" id="save">save proof</li>'
		// 						'<li class="menuitem" id="options">options</li>'
		// 					'</ul>'
		// 					'<div id="tab"><p>menu</p></div>'
		// 				'</div>';

		// 	$(this).append(win);
		// 	return $.activate();


		// }

	});

})(jQuery);
