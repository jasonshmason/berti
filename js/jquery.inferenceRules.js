/*
	bertie
		Logic functions for Rules Of Inference
 */
/**
 * @author Jason Campbell
 * @email  jason@jasonscampbell.com
**/

(function(jQuery){
	jQuery.fn.extend({

		a : function(p){ // auxiliary assumption

										//create auxiliary assumption html structure
			$(this).closest('li').drawAssumption(p);

							// null = enter auxiliary assumption body
			return discharge(null);

		},
		ip : function(p){ // indirect proof
		/*
		p
		-p
		thus -Q (-Q can be anything)
		*/


			var $this=$(this),
				arg=$.bertie.argument;

			var l=p.l1.split('-'),	// first and last line nums of assumption
				reductio=false,
				a=$.bertie.assumptions[l[0]]; // the assumption's sub-obj key = line num of aux assump. head


			l1=a[l[0]];
			l2=a[l[1]];
			tl=arg[p.tl];


			if(!(l1===$this.invertTruth(tl))){ // does thesis === assumptions negation?
			 	$this.error('misapplication of rule: thesis does not follow from assumption');
			 	return;
			}


			//iterate subderivation
			$.each(a, function(k,v){

				if(v===$this.invertTruth(l2))
					reductio=true;

			});

			if(reductio)
				checkQED();
			else{
				$this.error('misapplication of rule: thesis does not follow from subderivation');
				return;
			}

			function checkQED(){

				arg[p.tl]===arg.c? // check current proof against target conclusion
					QED=true
				:
					QED=false;

				return discharge(QED);

			}


		},
		r : function(p){ // reiteration
		/*
			p
			thus p
		*/
			var $this=$(this),
			arg=$.bertie.argument;


			if($this.inAssumption(p)){ // if sited line inside auxiliary assumption
				$this.error('misapplication of rule: thesis does not follow from auxiliary assumption');
				return;
			}


			if(arg[p.l1]===arg[p.tl])
				checkQED();
			else{
				$this.error('misapplication of rule: thesis does not follow from sited statement');
				return;
			}

			function checkQED(){

				arg[p.tl]===arg.c? // check current proof against target conclusion
					QED=true
				:
					QED=false;

				return discharge(QED);

			}
		},
		dn : function(p){
		/*
		p		--p
		-		  -
		--p		  p
		*/
			var $this=$(this),
				arg=$.bertie.argument,
				thesis=false;
				pat=/^-{2}/;

			var l1=arg[p.l1],
				tl=arg[p.tl];

			if(pat.test(l1)){
				thesis=tl===$this.extraParen(l1.replace(pat,''))?true:false;
			}
			else if(pat.test(tl))
				thesis=l1===$this.extraParen(tl.replace(pat,''))?true:false;
			else
				thesis=false;
			
			if(thesis)
				checkQED();
			else{
					$this.error('misapplication of rule: thesis does not follow from sited line');
					return;
			}

			function checkQED(){

				arg[p.tl]===arg.c? // check current proof against target conclusion
					QED=true
				:
					QED=false;

				return discharge(QED);

			}


		},
		mp : function(p){ // modus ponens
		/*
			p > q
			p
			thus q
		*/

			var $this=$(this),
				arg=$.bertie.argument;

			if($this.inAssumption(p)){ // if sited line inside auxiliary assumption
				$this.error('misapplication of rule: thesis does not follow from auxiliary assumption');
				return;
			}
											//parse statements into constituents: left operand, right operand, operator

			var l1=$this.atomize(arg[p.l1]),
				l2=$this.atomize(arg[p.l2]);

//			if(l1.ro===arg[p.tl] && l2.lo===l1.lo){// if line 1 consequent = proof thesis
			if(l1.ro===arg[p.tl] && arg[p.l2]===l1.lo){// if line 1 consequent = proof thesis
				if(l1.op!=='>'){
					$this.error('misapplication of rule: rule does not apply to thesis central operator');
					return;
				}
				checkQED();			// statement 1 is splitee

			}
//			else if(l2.ro===arg[p.tl] && l1.lo===l2.lo){// if line 2 consequent = proof thesis
			else if(l2.ro===arg[p.tl] && arg[p.l1]===l2.lo){// if line 2 consequent = proof thesis

				if(l2.op!=='>'){
					$this.error('misapplication of rule: rule does not apply to thesis central operator');
					return;
				}
				checkQED();			// statement 2 is splitee

			}
			else
				$this.error('misapplication of rule: thesis does not follow from sited statements')


			function checkQED(){

				arg[p.tl]===arg.c? // check current proof against target conclusion
					QED=true
				:
					QED=false;

				return discharge(QED);

			}

		},
		cp : function(p){	// conditional proof
		/*
			p
			-
			q

			thus p>q
		*/
console.log(p);
			var $this=$(this),
				arg=$.bertie.argument,
				l1, l2;

				var tl=$this.atomize(arg[p.tl]);	// proof thesis line obj

				// if($.aux){	// if we are still inside assumption, get lines from proof obj (p)

				// 	l1=arg[p.l1],
				// 	l2=arg[p.l2];

				// }
				// else{	// if assumption discharged, get lines from assumtions sub-obj

					var l=p.l1.split('-'),	// first and last line nums of assumption
						a=$.bertie.assumptions[l[0]]; // the assumption's sub-obj key = line num of aux assump. head

					if(!a)
						$this.error('No line cited');

					l1=a[l[0]];
					l2=a[l[1]];

				// }

				if(l1===tl.lo && l2===tl.ro){	// assump. first line = thesis left and assump. second line = thesis right
				 	tl.op==='>'?
						checkQED()
					:
						$this.error('misapplication of rule: rule does not apply to thesis central operator');
				}
				else
					$this.error('misapplication of rule: thesis does not follow from sited statements');

				function checkQED(){

					arg[p.tl]===arg.c? // check current proof against target conclusion
						QED=true
					:
						QED=false;

					return discharge(QED);

				}

		},
		bi : function(p){ // "biconditional introduction" (longform Material Equivilance)
		/*
		p	Q
		-	-
		q	p
		thus p=q

		p:
			r	:	'bi',
			tl	:	5,
			l1	:	1-2,
			l2	:	3-4
		*/


			var $this=$(this),
				arg=$.bertie.argument;

			var tl=$this.atomize(arg[p.tl]),
				sd1=tl.lo+'>'+tl.ro, // p>q
				sd2=tl.ro+'>'+tl.lo; // q>p


			// sd={
			// 	r : 'cp',
			// 	tl : p.
			// }


		},
		mt : function(p){	//modus tollens
		/*
			p > q
			-q
			thus -p
		*/
			var $this=$(this),
				arg=$.bertie.argument;

			var l1=$this.atomize(arg[p.l1]),
				l2=$this.atomize(arg[p.l2]);

			var tl=arg[p.tl];


			if(tl===$this.invertTruth(l1.lo)){// thesis = -line 1 left :: line 1 = splitee

				if(l1.op!=='>'){
					$this.error('misapplication of rule: rule does not apply to thesis central operator');
					return;
				}

				if($this.invertTruth(l1.ro)===l2.lo){// -line 1 right = line 2
					checkQED();
				}
			}
			else if(tl==='-'+$this.invertTruth(l2.lo)){// thesis = -line 2 left :: line 2 = splitee

				if(l2.op!=='>'){
					$this.error('misapplication of rule: rule does not apply to thesis central operator');
					return;
				}

				if($this.invertTruth(l2.ro)===l1.lo){// -line 1 right = line 2
					checkQED();
				}

			}

			function checkQED(){

				arg[p.tl]===arg.c? // check current proof against target conclusion
					QED=true
				:
					QED=false;

				return discharge(QED);

			}

		},
		hs : function(p){	//hypothetical syllogism
		/*
			p > q
			q > r
			thus p > r
		*/

			var $this=$(this),
				arg=$.bertie.argument;

			var l1=$this.atomize(arg[p.l1]),
				l2=$this.atomize(arg[p.l2]),
				tl=$this.atomize(arg[p.tl]);

			if(l1.op!=='>' || l2.op !=='>'){
				$this.error('misapplication of rule: rule does not apply to statement central operator');
				return;
			}
			else if(tl.op!=='>'){
				$this.error('misapplication of rule: rule does not apply to thesis central operator');
				return;
			}

			if(l1.ro===l2.lo){
				if(l1.lo===tl.lo && l2.ro===tl.ro)
					checkQED();
				else
			 		$this.error('misapplication of rule: thesis does not follow from sited statements')

			}
			else if(l2.ro===l1.lo){
				if(l2.lo===tl.lo && l1.ro===tl.ro)
					checkQED();
				else
			 		$this.error('misapplication of rule: thesis does not follow from sited statements')

			}
			else
			 	$this.error('misapplication of rule: thesis does not follow from sited statements')

			function checkQED(){

				arg[p.tl]===arg.c? // check current proof against target conclusion
					QED=true
				:
					QED=false;

				return discharge(QED);

			}
		},
		ds : function(p){	// disjunctive syllogism
		/*
			p v q
			-p
			thus q
			------
			p v q
			-q
			thus p
		*/

			var $this=$(this),
				arg=$.bertie.argument;

			var l1=$this.atomize(arg[p.l1]),
				l2=$this.atomize(arg[p.l2]);

			var tl=arg[p.tl];


			if(l1.op==='v'){
				obj=l1;
				sub=l2.lo;
			}
			else if(l2.op==='v'){
				obj=l2;
				sub=l1.lo;
			}
			else{
				$this.error('misapplication of rule: rule does not apply to statement central operator');
			}


			 if($this.invertTruth(obj.lo)===sub){
			 	if(obj.ro===tl)
			 		checkQED();
			 	else
			 		$this.error('misapplication of rule: thesis does not follow from sited statements')
			 }
			 else if($this.invertTruth(obj.ro)===sub){
			 	if(obj.lo===tl)
			 		checkQED();
			 	else
			 		$this.error('misapplication of rule: thesis does not follow from sited statements')
			 }
			 else
			 	$this.error('misapplication of rule: thesis does not follow from sited statements')

			function checkQED(){

				arg[p.tl]===arg.c? // check current proof against target conclusion
					QED=true
				:
					QED=false;

				return discharge(QED);

			}

		},
		cd : function(p){	// constructive dilemma
		/*
			(p > q) & (r > s)
			p v r
			thus q v s
		*/

			var $this=$(this),
				arg=$.bertie.argument,
				obj, sub;

			var l1=$this.atomize(arg[p.l1]),
				l2=$this.atomize(arg[p.l2])
				tl=$this.atomize(arg[p.tl]);

			if(tl.op !== 'v'){
				$this.error('misapplication of rule: rule does not apply to thesis central operator');
				return;
			}

			if(l1.op==='&'){// l1 = splitee
				obj=l1;
				sub=l2;
			}
			else if(l2.op==='&'){
				obj=l2;
				sub=l1;
			}
			else{
				$this.error('misapplication of rule: rule does not apply to statement central operator');
			}

			var objL=$this.atomize(obj.lo),
				objR=$this.atomize(obj.ro);

			if(sub.lo===objL.lo && sub.ro===objR.lo){
				if (tl.lo===objL.ro && tl.ro===objR.ro)
					checkQED();
				else
					$this.error('conclusion does not meet rule archetype');
			}
			else
				$this.error('argument does not meet rule archetype');


			function checkQED(){

				arg[p.tl]===arg.c? // check current proof against target conclusion
					QED=true
				:
					QED=false;

				return discharge(QED);

			}
		},
		dd : function(p){	// destructive dilemma
		/*
			(p > q) & (r > s)
			-q v -s
			thus -p v -r
		*/

			var $this=$(this),
				arg=$.bertie.argument,
				obj, sub;

			var l1=$this.atomize(arg[p.l1]),
				l2=$this.atomize(arg[p.l2])
				tl=$this.atomize(arg[p.tl]);

			if(tl.op !== 'v'){
				$this.error('misapplication of rule: rule does not apply to thesis central operator');
				return;
			}

			if(l1.op==='&'){// l1 = splitee
				obj=l1;
				sub=l2;
			}
			else if(l2.op==='&'){
				obj=l2;
				sub=l1;
			}
			else{
				$this.error('misapplication of rule: rule does not apply to statement central operator');
			}

			var objL=$this.atomize(obj.lo),
				objR=$this.atomize(obj.ro);


			if($this.invertTruth(sub.lo)===objL.ro && $this.invertTruth(sub.ro)===objR.ro){
				if ($this.invertTruth(tl.lo)===objL.lo && $this.invertTruth(tl.ro)===objR.lo)
					checkQED();
				else
					$this.error('conclusion does not meet rule archetype');
			}
			else
				$this.error('argument does not meet rule archetype');


			function checkQED(){

				arg[p.tl]===arg.c? // check current proof against target conclusion
					QED=true
				:
					QED=false;

				return discharge(QED);

			}

		},
		simpl : function(p){	// simplification
		/*
			p & q
			thus p
		*/

			var $this=$(this),
				arg=$.bertie.argument;

			if($this.inAssumption(p)){ // if sited line inside auxiliary assumption
				$this.error('misapplication of rule: thesis does not follow from auxiliary assumption');
				return;
			}

			l1=$this.atomize(arg[p.l1]);
			tl=$this.extraParen(arg[p.tl]);

			if(l1.op!=='&'){
				$this.error('misapplication of rule: rule does not apply to statement central operator');
				return;
			}

			if((l1.lo===tl)||(l1.ro===tl))
				checkQED();
			else
				$this.error('misapplication of rule: thesis does not follow from sited statements')

			function checkQED(){

				arg[p.tl]===arg.c? // check current proof against target conclusion
					QED=true
				:
					QED=false;

				return discharge(QED);

			}

		},
		conj : function(p){	// conjunction
		/*
			p
			q
			thus p & q
		*/

			var $this=$(this),
				arg=$.bertie.argument;

			if($this.inAssumption(p)){ // if sited line inside auxiliary assumption
				$this.error('misapplication of rule: thesis does not follow from auxiliary assumption');
				return;
			}


			var l1=$this.atomize(arg[p.tl]),
				tl=l1.lo,
				tr=l1.ro,
				op=l1.op;

			var l1=arg[p.l1],
				l2=arg[p.l2];

			if(op!=='&'){
				$this.error('misapplication of rule: rule does not apply to thesis central operator');
				return;
			}

			if((l1===tl || l1===tr) && (l2===tl || l2===tr))
				checkQED();
			else
				$this.error('misapplication of rule: thesis does not follow from sited statements')

			function checkQED(){
				arg[p.tl]===arg.c? // check current proof against target conclusion
					QED=true
				:
					QED=false;

				return discharge(QED);

			}

		},
		add : function(p){	// addition
		/*
			p
			thus p v q
		*/

			var $this=$(this),
				arg=$.bertie.argument;

			var l1=$this.atomize(arg[p.tl]),
				tl=l1.lo,
				tr=l1.ro,
				op=l1.op;

			var l1=arg[p.l1],
				l2=arg[p.l2];

			if(op!=='v'){
				$this.error('misapplication of rule: rule does not apply to thesis central operator');
				return;
			}

			if(tl===l1 || tr===l1)
				checkQED();
			else
				$this.error('misapplication of rule: thesis does not follow from sited statements')

			function checkQED(){
				arg[p.tl]===arg.c? // check current proof against target conclusion
					QED=true
				:
					QED=false;

				return discharge(QED);

			}

		}
	});
})(jQuery);
