/**
 bertie
	check statement syntax against rule archetype

 * @author Jason Campbell
 * @email  jason@jasonscampbell.com

**/

// $(document).ready(function(){
// s='(((P&Q)>R)>(S>-(SvT)))&(R>-(U&W))';
// console.log($('#interface').atomize(s));
// });

(function(jQuery){

	jQuery.fn.extend({

	atomize : function(s){// parse statement, return operator and operands

		var $this=$(this);

		if(s===undefined){
			$this.error('missing line citation');
			return;
		}

//		s=s.replace(/--/g,"");// strip double neg
		s=$this.extraParen(s); // strip extraneous paren
		//-------------------s returned

//		s.match(/^\(/)? // if beginning paren
		s.match(/^-?\(/)? // if begin, 0 or 1 '-', '('
			s=splitArg(s, 1) // if statement is a negative compound
		:
			s=splitArg(s, 0);

		return s;	// return to rule function

		function splitArg(s, n){ // parse left statement, right statement and operator

			if(n){
				osc=0;
				st=s.split('');
				for(var i=0;i<st.length;i++){
					inc=i+1;
					st[i]==='('?osc--:st[i]===')'?osc++:osc=osc;

//					if(osc===0){
					if(osc===0 && inc>1){
						op=st[inc]; // central operator
						sl=st.slice(0,inc).join(''); // left statement
						sr=st.slice(inc+1, st.length).join(''); // right statement
						break;
					}

				}
			}
			else{

//				sl=s.match(/^-?[A-Z]{1}/).toString();
//				sl=s.match(/^-?\(*[A-Z]{1}/).toString();	// begin, 0 or 1 '-', 0+ '(', 1 of A-Z

				try{// try to atomize statement
					sl=s.match(/^-?[A-Z]{1}/).toString();
					op=s.match(/[>v&=\*!]{1}/).toString();
		 			sr=s.substring(s.indexOf(op)+1,s.length);
	 			}
	 			catch(err){// if statement should not be atomized : sl is statement
	 				op=sr=null;
	 				sl=s;
	 			}
			}

			try{
				sl=$this.extraParen(sl);
			}
			catch(err){
				console.log('syntax error');
				return;
			}

			if(sr)sr=$this.extraParen(sr);
			if(sl)sl=$this.extraParen(sl);

			el={
				op : op,
				lo : sl,
				ro : sr
			}

			return el;
		}

		// function extraParen(s){// strip extranious enclosing parenthesis

		// 	if(s.match(/^\(/) && s.match(/\)$/)){ // if enclosing paren

		// 		oParens=s.match(/\(/g).length;
		// 		cParens=s.match(/\)/g).length;
		// 		opLen=s.match(/[>v&=\*!]/g).length*2;

		// 		if((oParens + cParens)===opLen){ // if no. of paren = no. of operators*2 : enclosing paren are extraneous

		// 			s=s.replace(/^\(/,"");
		// 			s=s.replace(/\)$/,"");

		// 		}
		// 	}

		// 	return s;
		// }
	},
	extraParen : function(s){// strip extranious enclosing parenthesis

		//var s=$(this);

		if(s.match(/^\(/) && s.match(/\)$/)){ // if enclosing paren

			oParens=s.match(/\(/g).length;
			cParens=s.match(/\)/g).length;
			opLen=s.match(/[>v&=\*!]/g).length*2;

			if((oParens + cParens)===opLen){ // if no. of paren = no. of operators*2 : enclosing paren are extraneous

				s=s.replace(/^\(/,"");
				s=s.replace(/\)$/,"");

			}
		}

		return s;
	},

	checkOperator : function(sOp, rOp){

		return (sOp===rOp);
	},
	invertTruth : function(s){

		if(s.match(/^-/)) // if statement false
			s=makePos(s);
		else
			s=makeNeg(s);

		return s;

		function makePos(s){

			if(s.match(/^-\(/)) // if false statement is compound
				s=s.substring(1,s.length);
			else
				s=s.substring(1,s.length);

			return s;

		}
		function makeNeg(s){

			s.match(/[>v&=\*!]/g)?
				s='-('+s+')'// if positive statement is compound, include paren
			:
				s='-'+s;

			return s;

		}
	}

 	});

 })(jQuery);